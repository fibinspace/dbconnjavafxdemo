package sample.Database;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class DBUtil {
    private static String jdbcUrl;
    private static boolean isInitialized;

    private static void initialize() {
        if(!isInitialized) {
            try {
                Properties properties = new Properties();
                InputStream source = DBUtil.class.getResourceAsStream("jdbc.properties");
                properties.load(source);
                source.close();
                Class.forName(properties.getProperty("driverClass"));
                jdbcUrl = properties.getProperty("jdbcUrl");
                isInitialized = true;
            } catch (Exception e)
            {
                System.out.println("Error while initializing JDBC");
            }
        }
    }

    public static Connection getConnection() throws Exception
    {

        initialize(); // metoda jest wywolana tylko raz

        return DriverManager.getConnection(jdbcUrl);
    }

    public static void Close(Statement stm) throws Exception
    {
        if(stm != null)
            stm.close();
    }

    public static void Close(Connection conn) throws Exception
    {
        if(conn != null)
            conn.close();
    }

    public static void Close(ResultSet rs) throws Exception
    {
        if(rs != null)
            rs.close();
    }
}
